import { HashRouter as Router, Redirect, Route, Switch } from "react-router-dom"
import { ProvideAuth } from "./contexts/AuthContext"
import { LayoutAuth } from "./Layout"
import { PrivateRoute } from "./routes/PrivateRoute"
import { routes } from "./routes/routes"

export const App = () => {
  const routesList = () => {
    const routesArray = []
    routes.forEach(route => {
      if (!route.private) routesArray.push(<Route key={route.name} exact path={route.path} render={() => <LayoutAuth><route.component /></LayoutAuth>} />)
      if (route.private) routesArray.push(<PrivateRoute key={route.name} exact path={route.path} component={route.component} />)
    })
    return routesArray
  }

  return (
    <Router>
      <ProvideAuth>
        <Switch>
          {routesList()}
          <Redirect from='/' to='/chat' />
        </Switch>
      </ProvideAuth>
    </Router>
  )
}
