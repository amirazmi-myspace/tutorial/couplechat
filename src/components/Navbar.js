import { useState } from "react"
import { Link, useHistory } from "react-router-dom"
import { useAuth } from "../contexts/AuthContext"

export const Navbar = () => {

  const history = useHistory()

  const { logout } = useAuth()

  const menu =()=>{
    
  }

  return (
    <>
      <div className='flex justify-between items-center bg-green-600 py-3 px-14 text-gray-100 shadow-md'>
        <Link to='/' className='cursor-pointer uppercase tracking-wider font-light'>Couple<span className='font-bold'>Chat</span></Link>
        {history.location.pathname === '/chat' && <i className="fas fa-toggle-on fa-lg cursor-pointer" onClick={logout}></i>}
      </div >
    </>
  )
}
